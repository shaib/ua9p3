extern crate unicode_bidi;
extern crate itertools;

use unicode_bidi::format_chars::{LRE, PDF, RLE};
use itertools::Itertools;

const PARA_SEP: &str = "\u{2029}";
const LINE_SEP: &str = "\u{2028}";

struct ParaFinder<'a> {
    text: &'a str,
    cur_pos: usize,
    find_para: fn(&str) -> Option<(&str, usize)>,
}

impl<'a> ParaFinder<'a> {
    fn new(text: &'a str, find_para: fn(&str) -> Option<(&str, usize)>) -> ParaFinder {
        ParaFinder {
            text,
            cur_pos: 0,
            find_para,
        }
    }
    // helper
    fn next_para(&self) -> Option<(&'a str, usize)> {
        (self.find_para)(&self.text[self.cur_pos..])
    }
}

impl<'a> Iterator for ParaFinder<'a> {
    type Item = &'a str;
    fn next(&mut self) -> Option<Self::Item> {
        if let Some((para, skip)) = self.next_para() {
            self.cur_pos += skip;
            Some(para)
        } else {
            None
        }
    }
}

pub fn get_dir_markers(text: &str) -> (char, char) {
    const LTR: (char, char) = (LRE, PDF);
    const RTL: (char, char) = (RLE, PDF);
    for chr in text.chars() {
        match unicode_bidi::bidi_class(chr) {
            // Add embedding chars RLE, RLO?
            unicode_bidi::BidiClass::R | unicode_bidi::BidiClass::AL => {
                return RTL;
            }
            // Add embedding chars LRE, LRO?
            unicode_bidi::BidiClass::L => {
                return LTR;
            }
            unicode_bidi::BidiClass::B => {
                panic!("Found a paragraph breaker in the line");
            }
            // Add special handling for isolates?
            _ => {}
        }
    }
    LTR  // Default, if no direction found
}

pub fn find_next_para(text: &str) -> Option<(&str, usize)> {
    let mut line_start = 0;
    let mut line_end;
    let mut empty = true;
    let mut rest = text;
    while line_start < text.len() {
        let split = &rest.find('\n').map(|i| i + 1).unwrap_or(rest.len());
        line_end = line_start + split;
        let line = &text[line_start..line_end];
        if line.trim().is_empty() && !empty {
            return Some((&text[..line_start], line_end));
        } else if !line.trim().is_empty() {
            empty = false;
        }
        // Either line wasn't empty, or the para is still empty
        line_start = line_end;
        rest = &text[line_end..];
    }
    if empty {
        None
    } else {
        // Gone through whole text, couldn't find an empty line
        Some((text, text.len()))
    }
}

struct Explicator<'a> {
    finder: ParaFinder<'a>,
}

impl<'a> Explicator<'a> {
    fn new(text: &'a str, find_para: fn(&str) -> Option<(&str, usize)>) -> Explicator {
        let finder = ParaFinder::new(text, find_para);
        Explicator { finder }
    }
}

impl<'a> Iterator for Explicator<'a> {
    type Item = String;
    fn next(&mut self) -> Option<Self::Item> {
        if let Some(para) = self.finder.next() {
            let (open, close) = get_dir_markers(para);
            // TODO find way to join the iterator
            let standard_para = para.lines().into_iter().collect::<Vec<&str>>().join(LINE_SEP);
            let p = format!("{}{}{}", open, standard_para, close);
            Some(p)
        } else {
            None
        }
    }
}

pub fn iter_join<'a>(i:&'a mut dyn Iterator<Item=&'a str>, sep:&'a str) -> String {
    i.intersperse(sep).collect::<String>()
}

pub fn explicate(text: &str) -> String {
    let explicator = Explicator::new(text, find_next_para);
    explicator.collect::<Vec<String>>().join(PARA_SEP)
}

#[cfg(test)]
mod tests {
    #[test]
    fn one_line_ltr() {
        let text = "Hello, world!";
        assert_eq!(::explicate(text), format!("{}{}{}", ::LRE, text, ::PDF));
    }

    #[test]
    fn two_para_ltr() {
        let text1 = "Hello, \n world!\n";
        let text2 = "Goodbye, world!\n";
        let text = vec![text1, text2].join("\n");
        let explicated = vec![text1, text2]
            .iter()
            .map(|x| format!("{}{}{}", ::LRE, x[..x.len()-1].replace("\n", ::LINE_SEP), ::PDF))
            .collect::<Vec<String>>()
            .join(::PARA_SEP);

        assert_eq!(::explicate(&text), explicated)
    }
}
