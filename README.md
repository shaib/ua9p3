## An attempt to fix Unicode BiDi

The Unicode BiDi Algorithm is an impressive achievement. It engages
with a hard problem -- the handling of text whose logical order needs
to be separate from its presentation order, and sets rules for
processing and presenting it. These rules have been utilized in the
last decades by millions of users around the world, to great
benefit. One cannot imagine using computers with Arabic, Hebrew,
Amharic or Persian without it.

The standard was built to be used with Higher Level Protocols, an
example of which is HTML. In HTML, each element has a base
directionality, and given such a directionality, the Unicode BiDi
Algorithm works wonderfully, specifying exactly how bi-directional
text should be rendered.

But sadly, the UBA is broken -- by design -- when dealing with Plain
Text.  The [standard](https://unicode.org/reports/tr9/) describes how
to set the base directionality of a paragraph (P1-P3), but allows
Higher Level Protocols to override that. And while it says how a
Higher Level Protocol may affect rendering, it stays silent on the
question of what constitutes a Higher Level Protocol. This is
explained in the [Unicode BiDi
FAQ](http://unicode.org/faq/bidi.html#bidi7a), in the last two
questions (at the time this document is written). The answers to these
questions dictate that any program may render plain text using any
base directionality, and still be standard-conformant.

This is a problem, because it creates ambiguity. As an example, a few
years ago, IBM bought Red Hat. In Hebrew, it is quite common to write
the English names of companies in English, especially if the name is
an acronym. So this would be written (remember, in Hebrew the line
begins on the right side):

```
	Red Hat קנתה את IBM
```

But any program is at liberty, according to the standard, to render it as

```
	IBM קנתה את Red Hat 
```

Luckily, we can fix the problem by utilizing the Unicode standard
itself. The standard includes directionality control characters, which can
be used to set the directionality explicitly.

This library includes tools for using these characters in order to make
plain-text viable as a form of text representation, even in the presence
of bidirectionality.
